# README #

## webots_ros_ws - Little Helper 8 (LH8)##
(Note this was made with catkin_make)

![LH8 - Littlehelper 8](/src/webots_ros/images/LH8webot.png)

### Test1: To run object grabbing in different orientation: ###

```
1. roscore (remember to source devel/setup.bash)

2. open webots LH8_webots1.wbt (The files can be found on webots_ros -> project -> worlds.)

3. rosrun webots_ros Little_helper_8key_working 

4. roslaunch webots_ros ur3e.launch or roslaunch webots_ros ur3e_joint_limited.launch

5. roslaunch ur3_e_moveit_config ur3_e_moveit_planning_execution.launch

6. rosrun webots_ros blind_pick_horizontal.py  -- to grab the center object
6. rosrun webots_ros blind_pick_straight.py  -- to grab the left object
6. rosrun webots_ros blind_pick_vertical.py -- to grab the right object
```

![Test 1](/src/webots_ros/images/Test1.png)

### Test2: To run human follow: ###
```
1. roscore (remember to source devel/setup.bash)

2. open webots LH8_webots2.wbt (The files can be found on webots_ros -> project -> worlds.)

3. rosrun webots_ros Little_helper_8key

4. roslaunch webots_ros ur3e.launch or roslaunch webots_ros ur3e_joint_limited.launch 

5. roslaunch ur3_e_moveit_config ur3_e_moveit_planning_execution.launch

6. rosrun webots_ros human_follow.py
```

![Test 2](/src/webots_ros/images/Humanfollow.png)

### Test3: To run object grab from human: ###
```
1. roscore (remember to source devel/setup.bash)

2. open webots LH8_webots3.wbt (The file can be found on webots_ros -> project -> worlds.)

3. rosrun webots_ros Little_helper_8key_working

4. roslaunch webots_ros ur3e_joint_limited.launch (has been tested it fails since there isnt limits on the joints - roslaunch webots_ros ur3e.launch)

5. roslaunch ur3_e_moveit_config ur3_e_moveit_planning_execution.launch

6. rosrun webots_ros blind_human_object_pick.py
```

![Grab object from human](/src/webots_ros/images/Humancan.png)

### For gmapping (merging the LiDARS) localisation with GPS & IMU: ###
```
1. roscore (remember to source devel/setup.bash)

2. open webots LH8_webots4.wbt (The file can be found on webots_ros -> project -> worlds.)

3. rosrun webots_ros Little_helper_8key_working 

5. roslaunch webots_ros ur3e.launch or roslaunch webots_ros ur3e_joint_limited.launch

6. roslaunch ira_laser_tools laserscan_multi_merger_rplidar.launch #(dont combine these files....launch separetely)

7. roslaunch ira_laser_tools laserscan_multi_merger.launch #(dont combine these files....launch separetely)

8. rosrun webots_ros keyboard_teleop_LH8_working 

9. rosrun gmapping slam_gmapping base_frame_:=lh8_base_link scan:=scan_multi

10. rosrun rviz rviz (check rqt to see frames. Sometimes shift from map to odom frame)
```

![Gmapping and localisation with GPS & IMU](/src/webots_ros/images/Gmap.PNG)
![TF tree](/src/webots_ros/images/TFtree.png)

### Object segmentation, Detection and Classification test: ###
```

 - OpenCV and Pycharm is needed
 
 - Basically the webots wbt for this test is to take screenshots of different angles/distance of circles, squares, triangles and rectangles. 
 
 - The python file (Object-Locate.py) will try and classify whether its a circle or a square.
 
 - Meaning for this implementation is to match the object recognition/Darknet-Yolo. 

 - Still under development...
```

![Object segmentation, Detection and Classification](/src/webots_ros/images/object.gif)


### For other (commands): ###
```

 - roscore (remember to source devel/setup.bash)

 - Open any webots world (wbt) for LH8. The files can be found on webots_ros -> project folder. 
   There is also another repository V1 to V6 can be found from - https://bitbucket.org/aauomniplatform/webots/src/master/
   
 - For keyboard teleop:  rosrun webots_ros keyboard_teleop_LH8_working 

 - For obstacle avoidance using LiDARS and motor velocity: rosrun webots_ros Little_helper_8auto
```
 




### For UR3e Modelling ###

- https://bitbucket.org/aauomniplatform/ur3e-modelling/src/master/

### For Youtube Test videos: ###

- https://www.youtube.com/playlist?list=PLSG9gXgVHC2OaHaly2AfDesKk5W0MFBX2


### For help with webots ros: ### 
```
Use webots_ros repository for inspiration. - https://github.com/cyberbotics/webots_ros

Also check out there website and Discord channel.
```
--------------------------------------------------
## Authors

Ditte Damgaard Albertsen:	        dalber16@student.aau.dk

Rahul Ravichandran:			rravic19@student.aau.dk

Rasmus Thomsen:				rthoms16@student.aau.dk

Mathiebhan Mahendran:			mmahen15@student.aau.dk

Alexander Staal:			astaal16@student.aau.dk

Carolina Bucle Infinito:                cgomez19@student.aau.dk

----------------------------------------------------
