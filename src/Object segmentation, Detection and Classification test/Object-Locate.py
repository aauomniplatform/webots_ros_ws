"""
Red: (hue)
-------------------
Create a second mask with values:
lower = 0 - 30 (Second mask for this one named mask 1, mask1 = mask1 + mask2)
upper = 150 - 180

Green: (hue)
lower = 40
upper = 75 / maybe 80?

Blue: (hue)
lower = 100
upper = 140
"""

import numpy as np
import cv2
import time


# ---------------------------------- BLOB FILTERING BASED ON AREA AND SHAPE --------------------------------------------
def get_contours(img_input, shape):
    contours, hierarchy = cv2.findContours(img_input, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    for cnt in contours:  # For all our found contours
        area = cv2.contourArea(cnt)
        if area > 500:  # 500 pixels
            shape_likeness = cv2.matchShapes(shape[0], cnt, 1, 0.0)  # Returns a float
            print(shape_likeness)
            if shape_likeness <= 0.08:  # We can set this even lower with better blobs
                cv2.drawContours(img, cnt, -1, (0, 0, 255), 3)


# --------------------------------------------- FOR HSV ADJUSTMENT -----------------------------------------------------
# For making our trackbars work we need a function to define what happens when we move the slider
# Since we just read the slider position directly we don't need to fill the function (just pass)
def empty(empty_arg):
    pass


# Creating the trackbars in a separate window
cv2.namedWindow("Trackbars")
cv2.resizeWindow("Trackbars", 640, 240)
cv2.createTrackbar("Saturation MIN", "Trackbars", 0, 255, empty)
cv2.createTrackbar("Saturation MAX", "Trackbars", 0, 255, empty)
cv2.createTrackbar("Value MIN", "Trackbars", 0, 255, empty)
cv2.createTrackbar("Value MAX", "Trackbars", 0, 255, empty)


# ---------------------------------------- FOR SHAPE DETECTOR ----------------------------------------------------------
# We load in some shapes
triangle = cv2.imread("Shapes/Triangle.png", 0)  # 0 = greyscale
square = cv2.imread("Shapes/Square.png.png", 0)
rectangle = cv2.imread("Shapes/Rectangle.png", 0)
circle = cv2.imread("Shapes/Circle.png", 0)

# We do a threshold
ret0, thresh0 = cv2.threshold(triangle, 50, 255, 0)
ret1, thresh1 = cv2.threshold(square, 50, 255, 0)
ret2, thresh2 = cv2.threshold(rectangle, 50, 255, 0)
ret3, thresh3 = cv2.threshold(circle, 50, 255, 0)

# Find the contours
contours_triangle, hierarchy0 = cv2.findContours(thresh0, 2, 1)
contours_square, hierarchy1 = cv2.findContours(thresh1, 2, 1)
contours_rectangle, hierarchy2 = cv2.findContours(thresh2, 2, 1)
contours_circle, hierarchy3 = cv2.findContours(thresh3, 2, 1)


# ---------------------------------------------------- MAIN PROGRAM ----------------------------------------------------

# WE MANAGE SOME INPUTS FROM THE USER
color = input("Please enter desired color (r, g, b): ")
shape = input("Please enter desired shape (t, s, r, c): ")

# We set the hue value based on color input
if color == 'r':
    h_min_zd = 0
    h_max_zd = 30
    h_min = 150
    h_max = 180
elif color == 'g':
    h_min = 45
    h_max = 75
else:
    h_min = 100
    h_max = 140


# We set the shape input based on shape input
if shape == 't':
    input_shape = contours_triangle
elif shape == 's':
    input_shape = contours_square
elif shape == 'r':
    input_shape = contours_rectangle
else:
    input_shape = contours_circle


# We take output from webcam
cap = cv2.VideoCapture(0)

# We give some time for the camera to warm-up!
time.sleep(3)

# While we keep receiving frames we run the loop
while cap.isOpened():

    # cap.read() returns two things. The video frame and a bool (True, False). 'ret' contains the bool
    ret, img = cap.read()

    # Converting the color space from BGR to HSV
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # Manually setting the saturation and value though trackbars
    s_min = cv2.getTrackbarPos("Saturation MIN", "Trackbars")
    s_max = cv2.getTrackbarPos("Saturation MAX", "Trackbars")
    v_min = cv2.getTrackbarPos("Value MIN", "Trackbars")
    v_max = cv2.getTrackbarPos("Value MAX", "Trackbars")

    # Generating mask to detect blue color
    # All the HSV values that falls within the rage are set to 1, otherwise 0
    lower = np.array([h_min, s_min, v_min])
    upper = np.array([h_max, s_max, v_max])
    mask1 = cv2.inRange(hsv, lower, upper)

    # Since red goes from 0-30 & 150-180 degrees on the HSV color wheel we make a second mask
    if color == 'r':
        lower = np.array([h_min_zd, s_min, v_min])
        upper = np.array([h_max_zd, s_max, v_max])
        mask2 = cv2.inRange(hsv, lower, upper)
        mask1 = mask1 + mask2

    # Refining the mask corresponding to the detected color
    mask1 = cv2.morphologyEx(mask1, cv2.MORPH_OPEN, np.ones((3, 3), np.uint8), iterations=2)
    mask1 = cv2.dilate(mask1, np.ones((5, 5), np.uint8), iterations=1)

    # Generating the final output
    # The mask is a binary image
    res = mask1

    # We enter in our color segmented image (mask1) and our desired shape
    get_contours(mask1, input_shape)

    cv2.imshow("Output", res)
    cv2.imshow("Normal image", img)

    # Press 'q' to end the program
    if cv2.waitKey(1) & 0XFF == ord('q'):
        break
