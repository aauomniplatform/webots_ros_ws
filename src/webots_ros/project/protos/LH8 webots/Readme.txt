webots LH8 cheatsheet


Sensor names
- Distance sensors 	= (distf, distb, distr, distl) (front, back, right, left)
- Lidars		= (lidar1, lidar2)
- Cordinate sensor	= GPS
- IMU			= IMU
- UR5e			= UR5

wheel configuration
|-------front-------|
|lidar		    |
|  2		1   |
|		    |
|		    |
|		    |
|		    |
|		    |
|		    |
|		    |
|  4		 3  |
|	       lidar|
|--------back-------|
