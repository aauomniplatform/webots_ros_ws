#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Range.h>
#include <signal.h>
#include <std_msgs/String.h>
#include <tf/transform_broadcaster.h>
#include "ros/ros.h"
#include <webots_ros/get_bool.h>
#include <webots_ros/get_float.h>
#include <webots_ros/get_int.h>
#include <webots_ros/get_string.h>
#include <webots_ros/get_uint64.h>
#include <webots_ros/set_bool.h>
#include <webots_ros/set_float.h>
#include <webots_ros/set_float_array.h>
#include <webots_ros/set_int.h>
#include <webots_ros/set_string.h>
#include <sensor_msgs/MagneticField.h>
#include <webots_ros/RecognitionObject.h>

#define TIME_STEP 32
#define NMOTORS 4

ros::NodeHandle *n;

static std::vector<float> lidarValues;

static int controllerCount;
static std::vector<std::string> controllerList;
static std::vector<unsigned char> imageColor;
static std::vector<unsigned char> imageColors;

ros::ServiceClient timeStepClient;
webots_ros::set_int timeStepSrv;

static const char *motorNames[NMOTORS] = {"wheel1", "wheel2", "wheel3", "wheel4"};

static double accelerometerValues[3] = {0, 0, 0};
static double compassValues[3] = {0, 0, 0};
static double GPSValues[3] = {0, 0, 0};
static double GyroValues[3] = {0, 0, 0};
static double inertialUnitValues[4] = {0, 0, 0, 0};
static double touchSensorValues[3] = {0, 0, 0};

static double cameraObject[3] = {0, 0, 0};
static double cameraObjects[4] = {0, 0, 0, 0};
static double cameraObjectss[3] = {0, 0, 0};
static double cameraObjectsss[3] = {0, 0, 0};
static double cameraObjectssss[3] = {0, 0, 0};


void broadcastTransform() {
  static tf::TransformBroadcaster br;
  tf::Transform transform;

  //////comment when not using gmapping
  transform.setOrigin( tf::Vector3(0, 0, 0));
  transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "odom"));
  //////

  transform.setOrigin(tf::Vector3(GPSValues[0], -GPSValues[1], GPSValues[2]));
  tf::Quaternion q(inertialUnitValues[0], inertialUnitValues[1], inertialUnitValues[2], inertialUnitValues[3]);
  transform.setRotation(q);

  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "odom", "lh8_base_link"));
  transform.setOrigin( tf::Vector3(0, 0, 0));
  transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "lh8_base_link", "Littlehelper8/inertial_unit_lh8"));
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "lh8_base_link", "Littlehelper8/gps_lh8"));
  transform.setOrigin( tf::Vector3(0.335, -0.229, 0.125) );
  q.setRPY(3.14, 0, 0);
  transform.setRotation(q);   //transform.setRotation( tf::Quaternion(0, 0, 0.25, 0) );
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "lh8_base_link", "Littlehelper8/LDS_1"));
  transform.setOrigin( tf::Vector3(-0.335, 0.229, 0.125) );
  q.setRPY(3.14, 0, 3.14);
  transform.setRotation(q);   //transform.setRotation( tf::Quaternion(0, 0, 0, 0.25) );
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "lh8_base_link", "Littlehelper8/LDS_2"));
  transform.setOrigin( tf::Vector3(0.249, -0.242, -0.069) );
  transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "lh8_base_link", "Littlehelper8/wheel1"));
  transform.setOrigin( tf::Vector3(0.249, 0.242, -0.069) );
  transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "lh8_base_link", "Littlehelper8/wheel2"));
  transform.setOrigin( tf::Vector3(-0.249, -0.242, -0.069) );
  transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "lh8_base_link", "Littlehelper8/wheel3"));
  transform.setOrigin( tf::Vector3(-0.249, 0.242, -0.069) );
  transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "lh8_base_link", "Littlehelper8/wheel4"));
  transform.setOrigin( tf::Vector3(0.4, 0, 0.59) );
  q.setRPY(3.14, 0, 0);
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "lh8_base_link", "/Littlehelper8/camera"));
  transform.setOrigin( tf::Vector3(0.28, 0.0, 0.66) );
  transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "lh8_base_link", "base_link"));
  // transform.setOrigin( tf::Vector3(0, 0.0, 0) );
  // transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "tool0", "UR3e/ROBOTIQ3fGripper"));
  // transform.setOrigin( tf::Vector3(0, 0.0, 0) );
  // transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "tool0", "UR3e/distancesensor"));
  // transform.setOrigin( tf::Vector3(0, 0.02, -0.09) );
  // transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "tool0", "UR3e/MultiSensearm"));
  // transform.setOrigin( tf::Vector3(0, 0, 0) );
  // transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/tool0", "/UR3egrip/finger_middle_joint_1_sensor"));
  // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/tool0", "/UR3egrip/finger_2_joint_1_sensor"));
  // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/tool0", "/UR3egrip/finger_1_joint_1_sensor"));
  // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/tool0", "/UR3egrip/finger_2_joint_2_sensor"));
  // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/tool0", "/UR3egrip/finger_1_joint_2_sensor"));
  // transform.setOrigin( tf::Vector3(0, 0, 0.056) );
  // transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/tool0", "/UR3egrip/camera"));
 }


void accelerometerCallback(const sensor_msgs::Imu::ConstPtr &values) {
  accelerometerValues[0] = values->linear_acceleration.x;
  accelerometerValues[1] = values->linear_acceleration.y;
  accelerometerValues[2] = values->linear_acceleration.z;
  broadcastTransform();
}

void compassCallback(const sensor_msgs::MagneticField::ConstPtr &values) {
  compassValues[0] = values->magnetic_field.x;
  compassValues[1] = values->magnetic_field.y;
  compassValues[2] = values->magnetic_field.z;
  broadcastTransform();
}

void GPSCallback(const sensor_msgs::NavSatFix::ConstPtr &values) {
  GPSValues[0] = values->latitude;
  GPSValues[1] = values->longitude;
  GPSValues[2] = values->altitude;
  broadcastTransform();
}

void gyroCallback(const sensor_msgs::Imu::ConstPtr &values) {
  GyroValues[0] = values->angular_velocity.x;
  GyroValues[1] = values->angular_velocity.y;
  GyroValues[2] = values->angular_velocity.z;
  broadcastTransform();
}

void inertialUnitCallback(const sensor_msgs::Imu::ConstPtr &values) {
  inertialUnitValues[0] = values->orientation.x;
  inertialUnitValues[1] = values->orientation.y;
  inertialUnitValues[2] = values->orientation.z;
  inertialUnitValues[3] = values->orientation.w;
  broadcastTransform();
}

void lidarCallback(const sensor_msgs::Image::ConstPtr &image) {
  broadcastTransform();
}
void lidarCallbacks(const sensor_msgs::Image::ConstPtr &image) {
 broadcastTransform();
}

void cameraCallback(const sensor_msgs::Image::ConstPtr &values) {
  int i = 0;
  imageColor.resize(values->step * values->height);
  for (std::vector<unsigned char>::const_iterator it = values->data.begin(); it != values->data.end(); ++it) {
    imageColor[i] = *it;
    i++;
  }
}

void cameraCallbacks(const sensor_msgs::Image::ConstPtr &values) {
  int i = 0;
  imageColors.resize(values->step * values->height);
  for (std::vector<unsigned char>::const_iterator its = values->data.begin(); its != values->data.end(); ++its) {
    imageColors[i] = *its;
    i++;
  }
}


void cameraRecognitionCallback(const webots_ros::RecognitionObject::ConstPtr &object) {
  cameraObject[0] = object->position.x;
  cameraObject[1] = object->position.y;
  cameraObject[2] = object->position.z;
  //ROS_INFO("Camera recognition base positions  x=%f y=%f z=%f (time: %d:%d).", cameraObject[0],
      // cameraObject[1], cameraObject[2], object->header.stamp.sec,
      //   object->header.stamp.nsec);

  cameraObjects[0] = object->orientation.x;
  cameraObjects[1] = object->orientation.y;
  cameraObjects[2] = object->orientation.z;
  cameraObjects[3] = object->orientation.w;
  //ROS_INFO("Camera recognition base orientation  x=%f y=%f z=%f w=%f (time: %d:%d).", cameraObjects[0],
            //  cameraObjects[1], cameraObjects[2], cameraObjects[3], object->header.stamp.sec,
            //    object->header.stamp.nsec);

  cameraObjectss[0] = object->position_on_image.x;
  cameraObjectss[1] = object->position_on_image.y;
  cameraObjectss[2] = object->position_on_image.z;
  //ROS_INFO("Camera recognition base position on image  x=%f y=%f z=%f (time: %d:%d).", cameraObjectss[0],
            //  cameraObjectss[1], cameraObjectss[2], object->header.stamp.sec,
              //  object->header.stamp.nsec);

  cameraObjectsss[0] = object->size_on_image.x;
  cameraObjectsss[1] = object->size_on_image.y;
  cameraObjectsss[2] = object->size_on_image.z;
  //ROS_INFO("Camera recognition base size on image  x=%f y=%f z=%f (time: %d:%d).", cameraObjectsss[0],
                        //    cameraObjectsss[1], cameraObjectss[2], object->header.stamp.sec,
                        //      object->header.stamp.nsec);

  //ROS_INFO("Camera recognition base saw a '%s' (time: %d:%d).", object->model.c_str(), object->header.stamp.sec,
          // object->header.stamp.nsec);
}

void cameraRecognitionCallbacks(const webots_ros::RecognitionObject::ConstPtr &object) {
  cameraObject[0] = object->position.x;
  cameraObject[1] = object->position.y;
  cameraObject[2] = object->position.z;
  //ROS_INFO("Camera recognition arm positions  x=%f y=%f z=%f (time: %d:%d).", cameraObject[0],
    //   cameraObject[1], cameraObject[2], object->header.stamp.sec,
      //   object->header.stamp.nsec);

  cameraObjects[0] = object->orientation.x;
  cameraObjects[1] = object->orientation.y;
  cameraObjects[2] = object->orientation.z;
  cameraObjects[3] = object->orientation.w;
  //ROS_INFO("Camera recognition arm orientation  x=%f y=%f z=%f w=%f (time: %d:%d).", cameraObjects[0],
            //  cameraObjects[1], cameraObjects[2], cameraObjects[3], object->header.stamp.sec,
            //    object->header.stamp.nsec);

  cameraObjectss[0] = object->position_on_image.x;
  cameraObjectss[1] = object->position_on_image.y;
  cameraObjectss[2] = object->position_on_image.z;
  //ROS_INFO("Camera recognition arm position on image  x=%f y=%f z=%f (time: %d:%d).", cameraObjectss[0],
          //    cameraObjectss[1], cameraObjectss[2], object->header.stamp.sec,
            //    object->header.stamp.nsec);

  cameraObjectsss[0] = object->size_on_image.x;
  cameraObjectsss[1] = object->size_on_image.y;
  cameraObjectsss[2] = object->size_on_image.z;
  //ROS_INFO("Camera recognition arm size on image  x=%f y=%f z=%f (time: %d:%d).", cameraObjectsss[0],
                      //      cameraObjectsss[1], cameraObjectss[2], object->header.stamp.sec,
                          //    object->header.stamp.nsec);

  //ROS_INFO("Camera recognition arm saw a '%s' (time: %d:%d).", object->model.c_str(), object->header.stamp.sec,
        //   object->header.stamp.nsec);
}


// catch names of the controllers availables on ROS network
void controllerNameCallback(const std_msgs::String::ConstPtr &name) {
  controllerCount++;
  controllerList.push_back(name->data);
  ROS_INFO("Controller #%d: %s.", controllerCount, controllerList.back().c_str());
  broadcastTransform();
}

void quit(int sig) {
  ROS_INFO("User stopped the 'Littlehelper8' node.");
  timeStepSrv.request.value = 0;
  timeStepClient.call(timeStepSrv);
  ros::shutdown();
  exit(0);
}

int main(int argc, char **argv) {
  std::string controllerName;
  // create a node named 'lh8' on ROS network
  ros::init(argc, argv, "Littlehelper8", ros::init_options::AnonymousName);
  ros::init(argc, argv, "UR3egrip", ros::init_options::AnonymousName);
  n = new ros::NodeHandle;

  signal(SIGINT, quit);

  // subscribe to the topic model_name to get the list of availables controllers
  ros::Subscriber nameSub = n->subscribe("model_name", 100, controllerNameCallback);
  while (controllerCount == 0 || controllerCount < nameSub.getNumPublishers()) {
    ros::spinOnce();
    ros::spinOnce();
    ros::spinOnce();
  }
  ros::spinOnce();

  timeStepClient = n->serviceClient<webots_ros::set_int>("Littlehelper8/robot/time_step");
  timeStepSrv.request.value = TIME_STEP;

  // if there is more than one controller available, it let the user choose
  if (controllerCount == 3)
    controllerName = controllerList[0];
  else {
    int wantedController = 0;
    std::cout << "Choose the # of the controller you want to use:\n";
    std::cin >> wantedController;
    if (3 <= wantedController && wantedController <= controllerCount)
      controllerName = controllerList[wantedController - 3];
    else {
      ROS_ERROR("Invalid number for controller choice.");
      return 1;
    }
  }
  ROS_INFO("Using controller: '%s'", controllerName.c_str());
  // leave topic once it is not necessary anymore
  nameSub.shutdown();


// init motors

//   ros::ServiceClient set_wheelsensors_client;
// webots_ros::set_int wheelsensors_srv;
// ros::Subscriber sub_wheelsensors_scan;
// set_wheelsensors_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/wheel1sensor/enable");
// wheelsensors_srv.request.value = TIME_STEP;
// set_wheelsensors_client.call(wheelsensors_srv);
//   ROS_INFO("wheel1sensor enabled.");

// set_wheelsensors_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/wheel2sensor/enable");
// wheelsensors_srv.request.value = TIME_STEP;
// set_wheelsensors_client.call(wheelsensors_srv);
//   ROS_INFO("wheel2sensor enabled.");

// set_wheelsensors_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/wheel3sensor/enable");
// wheelsensors_srv.request.value = TIME_STEP;
// set_wheelsensors_client.call(wheelsensors_srv);
//   ROS_INFO("wheel3sensor enabled.");

// set_wheelsensors_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/wheel4sensor/enable");
// wheelsensors_srv.request.value = TIME_STEP;
// set_wheelsensors_client.call(wheelsensors_srv);
//   ROS_INFO("wheel4sensor enabled.");

// set_wheelsensors_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/wheel1/torque_feedback_sensor/enable");
// wheelsensors_srv.request.value = TIME_STEP;
// set_wheelsensors_client.call(wheelsensors_srv);

// set_wheelsensors_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/wheel2/torque_feedback_sensor/enable");
// wheelsensors_srv.request.value = TIME_STEP;
// set_wheelsensors_client.call(wheelsensors_srv);

// set_wheelsensors_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/wheel3/torque_feedback_sensor/enable");
// wheelsensors_srv.request.value = TIME_STEP;
// set_wheelsensors_client.call(wheelsensors_srv);

// set_wheelsensors_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/wheel4/torque_feedback_sensor/enable");
// wheelsensors_srv.request.value = TIME_STEP;
// set_wheelsensors_client.call(wheelsensors_srv);

//////only for velocity control
  for (int i = 0; i < NMOTORS; ++i) {
    // position
    ros::ServiceClient set_position_client;
    webots_ros::set_float set_position_srv;
    set_position_client = n->serviceClient<webots_ros::set_float>(std::string("/Littlehelper8/") + std::string(motorNames[i]) +
                                                                  std::string("/set_position"));

    set_position_srv.request.value = INFINITY;
    if (set_position_client.call(set_position_srv) && set_position_srv.response.success)
      ROS_INFO("Position set to INFINITY for motor %s.", motorNames[i]);
    else
      ROS_ERROR("Failed to call service set_position on motor %s.", motorNames[i]);

    // speed
    ros::ServiceClient set_velocity_client;
    webots_ros::set_float set_velocity_srv;
    set_velocity_client = n->serviceClient<webots_ros::set_float>(std::string("/Littlehelper8/") + std::string(motorNames[i]) +
                                                                  std::string("/set_velocity"));

    set_velocity_srv.request.value = 0.0;
    if (set_velocity_client.call(set_velocity_srv) && set_velocity_srv.response.success == 1)
      ROS_INFO("Velocity set to 0.0 for motor %s.", motorNames[i]);
    else
      ROS_ERROR("Failed to call service set_velocity on motor %s.", motorNames[i]);
  }
//////////

  // enable lidar
  ros::ServiceClient set_lidar_client;
  webots_ros::set_int lidar_srv;
  ros::Subscriber sub_lidar_scan;
  set_lidar_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/LDS_1/enable");

  lidar_srv.request.value = TIME_STEP;
  if (set_lidar_client.call(lidar_srv) && lidar_srv.response.success) {
    ROS_INFO("Lidar1 enabled.");
    sub_lidar_scan = n->subscribe("Littlehelper8/LDS_1/range_image", 10, lidarCallback);   ///laser_scan/layer0
    ROS_INFO("Topic for lidar1 initialized.");
    while (sub_lidar_scan.getNumPublishers() == 0) {
    }
    ROS_INFO("Topic for lidar1 scan connected.");
  } else {
    if (!lidar_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable lidar1.");
    return 1;
  }

  set_lidar_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/LDS_2/enable");
  lidar_srv.request.value = TIME_STEP;
  if (set_lidar_client.call(lidar_srv) && lidar_srv.response.success) {
    ROS_INFO("Lidar2 enabled.");
    sub_lidar_scan = n->subscribe("Littlehelper8/LDS_2/range_image", 10, lidarCallbacks);  ///laser_scan/layer0
    ROS_INFO("Topic for lidar2 initialized.");
    while (sub_lidar_scan.getNumPublishers() == 0) {
    }
    ROS_INFO("Topic for lidar2 scan connected.");
  } else {
    if (!lidar_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable lidar2.");
    return 1;
  }

//  enable gps
  ros::ServiceClient set_gps_client;
  webots_ros::set_int gps_srv;
  ros::Subscriber sub_gps;
  set_gps_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/gps_lh8/enable");

  ros::ServiceClient sampling_period_gps_client;
  webots_ros::get_int sampling_period_gps_srv;
  sampling_period_gps_client = n->serviceClient<webots_ros::get_int>("Littlehelper8/gps_lh8/get_sampling_period");

  ros::ServiceClient gps_get_coordinate_system_client;
  webots_ros::get_int gps_get_coordinate_system_srv;
  gps_get_coordinate_system_client = n->serviceClient<webots_ros::get_int>("Littlehelper8/gps_lh8/get_coordinate_system");

  gps_srv.request.value = 32;
  if (set_gps_client.call(gps_srv) && gps_srv.response.success) {
    sub_gps = n->subscribe("Littlehelper8/gps_lh8/values", 1, GPSCallback);
    while (sub_gps.getNumPublishers() == 0) {
    }
    ROS_INFO("gps enabled.");
  } else {
    if (!gps_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable gps.");
    return 1;
  }
  sampling_period_gps_client.call(sampling_period_gps_srv);
  ROS_INFO("gps is enabled with a sampling period of %d.", sampling_period_gps_srv.response.value);
  gps_get_coordinate_system_client.call(gps_get_coordinate_system_srv);
  ROS_INFO("gps coordinate system type is: %d.", gps_get_coordinate_system_srv.response.value);

  //  enable gps for ped
  set_gps_client = n->serviceClient<webots_ros::set_int>("ped/gps/enable");

  sampling_period_gps_client = n->serviceClient<webots_ros::get_int>("ped/gps/get_sampling_period");

  gps_get_coordinate_system_client = n->serviceClient<webots_ros::get_int>("ped/gps/get_coordinate_system");

  gps_srv.request.value = 32;
  if (set_gps_client.call(gps_srv) && gps_srv.response.success) {
    sub_gps = n->subscribe("ped/gps/values", 1, GPSCallback);
    while (sub_gps.getNumPublishers() == 0) {
    }
    ROS_INFO("gps enabled.");
  } else {
    if (!gps_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable gps.");
    return 1;
  }
  sampling_period_gps_client.call(sampling_period_gps_srv);
  ROS_INFO("gps is enabled with a sampling period of %d.", sampling_period_gps_srv.response.value);
  gps_get_coordinate_system_client.call(gps_get_coordinate_system_srv);
  ROS_INFO("gps coordinate system type is: %d.", gps_get_coordinate_system_srv.response.value);




//  enable inertial unit
  ros::ServiceClient set_inertial_unit_client;
  webots_ros::set_int inertial_unit_srv;
  ros::Subscriber sub_inertial_unit;
  set_inertial_unit_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/inertial_unit_lh8/enable");

  ros::ServiceClient sampling_period_inertial_unit_client;
  webots_ros::get_int sampling_period_inertial_unit_srv;
  sampling_period_inertial_unit_client = n->serviceClient<webots_ros::get_int>("Littlehelper8/inertial_unit_lh8/get_sampling_period");

  inertial_unit_srv.request.value = 32;
  if (set_inertial_unit_client.call(inertial_unit_srv) && inertial_unit_srv.response.success) {
    sub_inertial_unit = n->subscribe("Littlehelper8/inertial_unit_lh8/roll_pitch_yaw", 1, inertialUnitCallback);
    while (sub_inertial_unit.getNumPublishers() == 0) {
    }
    ROS_INFO("Inertial unit enabled.");
  } else {
    if (!inertial_unit_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable inertial unit.");
    return 1;
  }

  sampling_period_inertial_unit_client.call(sampling_period_inertial_unit_srv);
  ROS_INFO("Inertial_unit is enabled with a sampling period of %d.", sampling_period_inertial_unit_srv.response.value);

  // enable camera
  ros::ServiceClient set_camera_client;
  webots_ros::set_int camera_srv;
  ros::Subscriber sub_camera;

  set_camera_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/camera/enable");
  camera_srv.request.value = 32;
  if (set_camera_client.call(camera_srv) && camera_srv.response.success) {
    ROS_INFO("camera base enabled.");
    sub_camera = n->subscribe("/Littlehelper8/camera/image", 1, cameraCallback);
    ROS_INFO("Topic for camera base initialized.");
    while (sub_camera.getNumPublishers() == 0) {
    }
    ROS_INFO("Topic for camera base connected.");
  } else {
    if (!camera_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable camera base.");
    return 1;
  }

    set_camera_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/camera/enable");
    camera_srv.request.value = 32;
    if (set_camera_client.call(camera_srv) && camera_srv.response.success) {
      ROS_INFO("camera arm enabled.");
      sub_camera = n->subscribe("/UR3egrip/camera/image", 1, cameraCallbacks);
      ROS_INFO("Topic for camera arm initialized.");
      while (sub_camera.getNumPublishers() == 0) {
      }
      ROS_INFO("Topic for camera arm connected.");
    } else {
      if (!camera_srv.response.success)
        ROS_ERROR("Sampling period is not valid.");
      ROS_ERROR("Failed to enable camera arm.");
      return 1;
    }

  ros::ServiceClient enable_camera_recognition_client;
  webots_ros::set_int camera_recognition_srv;
  ros::Subscriber sub_camera_recognition;

  enable_camera_recognition_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/camera/recognition_enable");
  camera_recognition_srv.request.value = 32;
  if (enable_camera_recognition_client.call(camera_recognition_srv) && camera_recognition_srv.response.success) {
    ROS_INFO("camera recognition base enabled.");
    sub_camera_recognition = n->subscribe("/Littlehelper8/camera/recognition_objects", 1, cameraRecognitionCallback);
    ROS_INFO("Topic for camera recognition base initialized.");
    while (sub_camera_recognition.getNumPublishers() == 0) {
    }
    ROS_INFO("Topic for camera recognition base connected.");
  } else {
    if (!camera_recognition_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable camera recognition base.");
    return 1;
  }

  enable_camera_recognition_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/camera/recognition_enable");
  camera_recognition_srv.request.value = 32;
  if (enable_camera_recognition_client.call(camera_recognition_srv) && camera_recognition_srv.response.success) {
    ROS_INFO("camera recognition arm enabled.");
    sub_camera_recognition = n->subscribe("/UR3egrip/camera/recognition_objects", 1, cameraRecognitionCallbacks);
    ROS_INFO("Topic for camera recognition arm initialized.");
    while (sub_camera_recognition.getNumPublishers() == 0) {
    }
    ROS_INFO("Topic for camera recognition arm connected.");
  } else {
    if (!camera_recognition_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable camera recognition arm.");
    return 1;
  }



  //turn on leds
 ros::ServiceClient set_led_client;
 webots_ros::set_int set_led_srv;
 set_led_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/led1/set_led");

 set_led_srv.request.value = 1;
 if (set_led_client.call(set_led_srv) && set_led_srv.response.success)
   ROS_INFO("LED set to 1.");
 else
   ROS_ERROR("Failed to call service set_led.");

 set_led_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/led2/set_led");

 set_led_srv.request.value = 1;
 if (set_led_client.call(set_led_srv) && set_led_srv.response.success)
   ROS_INFO("LED set to 2.");
 else
   ROS_ERROR("Failed to call service set_led.");

 set_led_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/led3/set_led");

 set_led_srv.request.value = 1;
 if (set_led_client.call(set_led_srv) && set_led_srv.response.success)
   ROS_INFO("LED set to 3.");
 else
   ROS_ERROR("Failed to call service set_led.");

 set_led_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/led4/set_led");

 set_led_srv.request.value = 1;
 if (set_led_client.call(set_led_srv) && set_led_srv.response.success)
   ROS_INFO("LED set to 4.");
 else
   ROS_ERROR("Failed to call service set_led.");


   //gripper enable
   ros::ServiceClient set_gripper_client;
    webots_ros::set_int gripper_srv;
    ros::Subscriber sub_gripper_unit;
    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/finger_1_joint_1_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/finger_1_joint_2_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/finger_2_joint_1_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/finger_2_joint_2_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/finger_middle_joint_1_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/finger_middle_joint_2_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    //new gripper

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/gripper_finger1_finger_tip_joint_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/gripper_finger1_inner_knuckle_joint_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/gripper_finger1_joint_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);


    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/gripper_finger1_joint/torque_feedback_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/gripper_finger1_inner_knuckle_joint/torque_feedback_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);


    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/gripper_finger2_finger_tip_joint/torque_feedback_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/gripper_finger2_inner_knuckle_joint_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/gripper_finger2_joint_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/gripper_finger2_joint/torque_feedback_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

    set_gripper_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/gripper_finger2_inner_knuckle_joint/torque_feedback_sensor/enable");
    gripper_srv.request.value = TIME_STEP;
    set_gripper_client.call(gripper_srv);

   ROS_INFO("You can now start the creation of the map using 'rosrun gmapping slam_gmapping "
           "base_frame_:=lh8_base_link scan:=scan_multi");

   ROS_INFO("You can now visualize the sensors output in rqt using 'rqt'.");


  // main loop
  while (ros::ok()) {
    if (!timeStepClient.call(timeStepSrv) || !timeStepSrv.response.success) {
      ROS_ERROR("Failed to call service time_step for next step.");
      break;
    }
    ros::spinOnce();
  }
  timeStepSrv.request.value = 0;
  timeStepClient.call(timeStepSrv);

  ros::shutdown();
  return 0;
}
