#! /usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from tf import transformations
from math import cos, sin, pi


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_tut', anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group = moveit_commander.MoveGroupCommander("manipulator")
display_trajectory_publisher = rospy.Publisher('move_group/display_planned_path',moveit_msgs.msg.DisplayTrajectory)

pose_target = geometry_msgs.msg.Pose()

q = transformations.quaternion_from_euler(0, 0, 0, "sxyz") # r p y

print "ref frame: %s" % group.get_planning_frame()

print "joint values: %s" % group.get_current_joint_values()

print "end effector pose: %s" % group.get_current_pose()

print "Robot current state: %s" % robot.get_current_state()

pose_target.orientation.x = q[0]
pose_target.orientation.y = q[1]
pose_target.orientation.z = q[2]
pose_target.orientation.w = q[3]
pose_target.position.x=0.3
pose_target.position.y=0.3
pose_target.position.z=0.2
group.set_pose_target(pose_target)


# group_variable_values = group.get_current_joint_values()
# group_variable_values[0] = 0
# group_variable_values[1] = -1.57
# group_variable_values[3] = -1.57
# group_variable_values[5] = 1.57
# group.set_joint_value_target(group_variable_values)


plan1 = group.plan()

group.go(wait=True)
# rospy.sleep(5)

moveit_commander.roscpp_shutdown()
