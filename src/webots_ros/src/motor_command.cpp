#include "ros/ros.h"
#include <webots_ros/Int32Stamped.h>
#include <webots_ros/set_float.h>
#include <webots_ros/set_int.h>
#include <webots_ros/robot_get_device_list.h>
#include <std_msgs/String.h>
#include <sensor_msgs/Imu.h>
#include <signal.h>
#include <stdio.h>

#define TIME_STEP 32

static int controllerCount;
static std::vector<std::string> controllerList;
static std::string controllerName;
static double tlposition = 0;
static double trposition = 0;
static double blposition = 0;
static double brposition = 0;

ros::ServiceClient topleftWheelClient;
webots_ros::set_float topleftWheelSrv;

ros::ServiceClient toprightWheelClient;
webots_ros::set_float toprightWheelSrv;

ros::ServiceClient bottomleftWheelClient;
webots_ros::set_float bottomleftWheelSrv;

ros::ServiceClient bottomrightWheelClient;
webots_ros::set_float bottomrightWheelSrv;

ros::ServiceClient timeStepClient;
webots_ros::set_int timeStepSrv;


// catch names of the controllers availables on ROS network
void controllerNameCallback(const std_msgs::String::ConstPtr &name) {
  controllerCount++;
  controllerList.push_back(name->data);
  ROS_INFO("Controller #%d: %s.", controllerCount, controllerList.back().c_str());
}


void motorvelCallback(const sensor_msgs::Imu::ConstPtr &value) {
  int send =1;
  trposition = value->orientation.x;
  tlposition = value->orientation.y;
  brposition = value->orientation.z;
  blposition = value->orientation.w;
  ROS_INFO("trposition: %f, tlposition: %f, brposition: %f, blposition: %f",trposition,tlposition,brposition,blposition);


  topleftWheelSrv.request.value = tlposition;
  toprightWheelSrv.request.value = trposition;
  bottomleftWheelSrv.request.value = blposition;
  bottomrightWheelSrv.request.value = brposition;
  if (send) {
    ROS_INFO("sending motor command");
    if (!topleftWheelClient.call(topleftWheelSrv) || !topleftWheelSrv.response.success)
      ROS_ERROR("Failed to send new position commands to the topleft wheel.");
    if (!toprightWheelClient.call(toprightWheelSrv) ||  !toprightWheelSrv.response.success)
      ROS_ERROR("Failed to send new position commands to the topright wheel.");
    if (!bottomleftWheelClient.call(bottomleftWheelSrv) || !bottomleftWheelSrv.response.success)
      ROS_ERROR("Failed to send new position commands to the bottomleft wheel.");
    if (!bottomrightWheelClient.call(bottomrightWheelSrv) || !bottomrightWheelSrv.response.success)
      ROS_ERROR("Failed to send new position commands to the bottomright wheel.");
  }
  return;
}

int main(int argc, char **argv) {
  // create a node named 'keyboard_teleop' on ROS network
  ros::init(argc, argv, "motor_command", ros::init_options::AnonymousName);
  ros::NodeHandle n;


  // subscribe to the topic model_name to get the list of availables controllers
  ros::Subscriber nameSub = n.subscribe("model_name", 100, controllerNameCallback);
  
  while (controllerCount == 0 || controllerCount < nameSub.getNumPublishers()) {
    ros::spinOnce();
    ros::spinOnce();
    ros::spinOnce();
  }
  ros::spinOnce();

  // if there is more than one controller available, let the user choose
  if (controllerCount == 1)
    controllerName = controllerList[0];
  else {
    int wantedController = 0;
    std::cout << "Choose the # of the controller you want to use:\n";
    std::cin >> wantedController;
    if (1 <= wantedController && wantedController <= controllerCount)
      controllerName = controllerList[wantedController - 1];
    else {
      ROS_ERROR("Invalid number for controller choice.");
      return 1;
    }
  }
  // leave topic once it's not necessary anymore
  nameSub.shutdown();

  toprightWheelClient = n.serviceClient<webots_ros::set_float>(controllerName + "/wheel1/set_velocity");
  topleftWheelClient = n.serviceClient<webots_ros::set_float>(controllerName + "/wheel2/set_velocity");
  bottomrightWheelClient = n.serviceClient<webots_ros::set_float>(controllerName + "/wheel3/set_velocity");
  bottomleftWheelClient = n.serviceClient<webots_ros::set_float>(controllerName + "/wheel4/set_velocity");
  timeStepClient = n.serviceClient<webots_ros::set_int>(controllerName + "/robot/time_step");
  timeStepSrv.request.value = TIME_STEP;

  
  ros::Subscriber motor_vel_sub = n.subscribe("motor_vel", 100, motorvelCallback);
  
  // main loop
  while (ros::ok()) {
    ros::spinOnce();
    if (!timeStepClient.call(timeStepSrv) || !timeStepSrv.response.success)
      ROS_ERROR("Failed to call service time_step for next step.");
  }


  timeStepSrv.request.value = 0;
  timeStepClient.call(timeStepSrv);
  ros::shutdown();
  return (0);
}
