#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Range.h>
#include <signal.h>
#include <std_msgs/String.h>
#include <tf/transform_broadcaster.h>
#include "ros/ros.h"

#include <webots_ros/get_bool.h>
#include <webots_ros/get_float.h>
#include <webots_ros/get_int.h>
#include <webots_ros/get_string.h>
#include <webots_ros/get_uint64.h>
#include <webots_ros/set_bool.h>
#include <webots_ros/set_float.h>
#include <webots_ros/set_float_array.h>
#include <webots_ros/set_int.h>
#include <webots_ros/set_string.h>
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/WrenchStamped.h>
#include <sensor_msgs/Illuminance.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/Range.h>
#include <signal.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/String.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/UInt8MultiArray.h>
#include <webots_ros/BoolStamped.h>
#include <webots_ros/Float64Stamped.h>
#include <webots_ros/Int32Stamped.h>
#include <webots_ros/Int8Stamped.h>
#include <webots_ros/RadarTarget.h>
#include <webots_ros/RecognitionObject.h>
#include <webots_ros/StringStamped.h>
#include <tf/transform_broadcaster.h>
#include <cstdlib>
#include "ros/ros.h"
#include <webots_ros/get_bool.h>
#include <webots_ros/get_float.h>
#include <webots_ros/get_int.h>
#include <webots_ros/get_string.h>
#include <webots_ros/get_uint64.h>
#include <webots_ros/set_bool.h>
#include <webots_ros/set_float.h>
#include <webots_ros/set_float_array.h>
#include <webots_ros/set_int.h>
#include <webots_ros/set_string.h>
#include <webots_ros/camera_get_focus_info.h>
#include <webots_ros/camera_get_info.h>
#include <webots_ros/camera_get_zoom_info.h>
#include <webots_ros/display_draw_line.h>
#include <webots_ros/display_draw_oval.h>
#include <webots_ros/display_draw_pixel.h>
#include <webots_ros/display_draw_polygon.h>
#include <webots_ros/display_draw_rectangle.h>
#include <webots_ros/display_draw_text.h>
#include <webots_ros/display_get_info.h>
#include <webots_ros/display_image_copy.h>
#include <webots_ros/display_image_delete.h>
#include <webots_ros/display_image_load.h>
#include <webots_ros/display_image_new.h>
#include <webots_ros/display_image_paste.h>
#include <webots_ros/display_image_save.h>
#include <webots_ros/display_set_font.h>
#include <webots_ros/field_get_bool.h>
#include <webots_ros/field_get_color.h>
#include <webots_ros/field_get_count.h>
#include <webots_ros/field_get_float.h>
#include <webots_ros/field_get_int32.h>
#include <webots_ros/field_get_node.h>
#include <webots_ros/field_get_rotation.h>
#include <webots_ros/field_get_string.h>
#include <webots_ros/field_get_type.h>
#include <webots_ros/field_get_type_name.h>
#include <webots_ros/field_get_vec2f.h>
#include <webots_ros/field_get_vec3f.h>
#include <webots_ros/field_import_node.h>
#include <webots_ros/field_remove.h>
#include <webots_ros/field_set_bool.h>
#include <webots_ros/field_set_color.h>
#include <webots_ros/field_set_float.h>
#include <webots_ros/field_set_int32.h>
#include <webots_ros/field_set_rotation.h>
#include <webots_ros/field_set_string.h>
#include <webots_ros/field_set_vec2f.h>
#include <webots_ros/field_set_vec3f.h>
#include <webots_ros/lidar_get_frequency_info.h>
#include <webots_ros/lidar_get_info.h>
#include <webots_ros/motor_set_control_pid.h>
#include <webots_ros/node_add_force_or_torque.h>
#include <webots_ros/node_add_force_with_offset.h>
#include <webots_ros/node_get_center_of_mass.h>
#include <webots_ros/node_get_contact_point.h>
#include <webots_ros/node_get_field.h>
#include <webots_ros/node_get_id.h>
#include <webots_ros/node_get_name.h>
#include <webots_ros/node_get_number_of_contact_points.h>
#include <webots_ros/node_get_orientation.h>
#include <webots_ros/node_get_parent_node.h>
#include <webots_ros/node_get_position.h>
#include <webots_ros/node_get_static_balance.h>
#include <webots_ros/node_get_status.h>
#include <webots_ros/node_get_type.h>
#include <webots_ros/node_get_velocity.h>
#include <webots_ros/node_remove.h>
#include <webots_ros/node_reset_functions.h>
#include <webots_ros/node_set_velocity.h>
#include <webots_ros/node_set_visibility.h>
#include <webots_ros/pen_set_ink_color.h>
#include <webots_ros/range_finder_get_info.h>
#include <webots_ros/receiver_get_emitter_direction.h>
#include <webots_ros/robot_get_device_list.h>
#include <webots_ros/robot_set_mode.h>
#include <webots_ros/robot_wait_for_user_input_event.h>
#include <webots_ros/save_image.h>
#include <webots_ros/speaker_play_sound.h>
#include <webots_ros/speaker_speak.h>
#include <webots_ros/supervisor_get_from_def.h>
#include <webots_ros/supervisor_get_from_id.h>
#include <webots_ros/supervisor_movie_start_recording.h>
#include <webots_ros/supervisor_set_label.h>
#include <webots_ros/supervisor_virtual_reality_headset_get_orientation.h>
#include <webots_ros/supervisor_virtual_reality_headset_get_position.h>

#define TIME_STEP 32
#define NMOTORS 4
#define MAX_SPEED 6.4
#define OBSTACLE_THRESHOLD 0.1
#define DECREASE_FACTOR 0.9
#define BACK_SLOWDOWN 0.9
#define BACK_SPEED -5.0

ros::NodeHandle *n;

static std::vector<float> lidarValues;

static int controllerCount;
static std::vector<std::string> controllerList;

ros::ServiceClient timeStepClient;
webots_ros::set_int timeStepSrv;

static const char *motorNames[NMOTORS] = {"wheel1", "wheel2", "wheel3", "wheel4"};

static double GPSValues[3] = {0, 0, 0};
static double inertialUnitValues[4] = {0, 0, 0, 0};

static int rplidarResolution = 180;
static int halfResolution = 90;
static double maxRange = 0.0;
static double rangeThreshold = 0;
static std::vector<double> braitenbergCoefficients;
static bool areBraitenbergCoefficientsinitialized = false;

// gaussian function
double gaussian(double x, double mu, double sigma) {
  return (1.0 / (sigma * sqrt(2.0 * M_PI))) * exp(-((x - mu) * (x - mu)) / (2 * sigma * sigma));
}

void updateSpeed() {
  // init dynamic variables
  double leftObstacle = 0.0, rightObstacle = 0.0, obstacle = 0.0;
  double speeds[NMOTORS];
  // apply the braitenberg coefficients on the resulted values of the rplidar
  // near obstacle sensed on the left side
  for (int i = 0; i < halfResolution; ++i) {
    if (lidarValues[i] < rangeThreshold)  // far obstacles are ignored
      leftObstacle += braitenbergCoefficients[i] * (1.0 - lidarValues[i] / maxRange);
    // near obstacle sensed on the right side
    int j = rplidarResolution - i - 1;
    if (lidarValues[j] < rangeThreshold)
      rightObstacle += braitenbergCoefficients[i] * (1.0 - lidarValues[j] / maxRange);
  }
  // overall front obstacle
  obstacle = leftObstacle + rightObstacle;
  // compute the speed according to the information on
  // obstacles
  if (obstacle > OBSTACLE_THRESHOLD) {
    const double speedFactor = (1.0 - DECREASE_FACTOR * obstacle) * MAX_SPEED / obstacle;
    speeds[0,3] = speedFactor * leftObstacle;
    speeds[1,2] = speedFactor * rightObstacle;
    speeds[2,1] = BACK_SLOWDOWN * speeds[0,3];
    speeds[3,0] = BACK_SLOWDOWN * speeds[1,2];
    speeds[0,1] = leftObstacle * rightObstacle; //comment out this for linear movement
    //speeds[2,3] = leftObstacle * rightObstacle;  //opitional

  } else {
    speeds[0] = MAX_SPEED;
    speeds[1] = MAX_SPEED;
    speeds[2] = MAX_SPEED;
    speeds[3] = MAX_SPEED;
    speeds[0,1,2,3] = BACK_SPEED; //comment out this for linear movement

   // wheels[0](rightSpeed);
   // wheels[1](leftSpeed);
   // wheels[2](leftSpeed);
   // wheels[3](rightSpeed);
  }
  // set speeds
  for (int i = 0; i < NMOTORS; ++i) {
    ros::ServiceClient set_velocity_client;
    webots_ros::set_float set_velocity_srv;
    set_velocity_client = n->serviceClient<webots_ros::set_float>(std::string("Littlehelper8/") + std::string(motorNames[i]) +
                                                                  std::string("/set_velocity"));
    set_velocity_srv.request.value = speeds[i];
    set_velocity_client.call(set_velocity_srv);
  }
}

void broadcastTransform() {
  static tf::TransformBroadcaster br;
  tf::Transform transform;
  transform.setOrigin(tf::Vector3(-GPSValues[2], GPSValues[0], GPSValues[1]));
  tf::Quaternion q(inertialUnitValues[0], inertialUnitValues[1], inertialUnitValues[2], inertialUnitValues[3]);
  q = q.inverse();
  transform.setRotation(q);
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "odom", "base_link"));
  transform.setIdentity();
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "Littlehelper8/LDS_1"));
  transform.setIdentity();
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "Littlehelper8/LDS_2"));
}

void GPSCallback(const sensor_msgs::NavSatFix::ConstPtr &values) {
  GPSValues[0] = values->latitude;
  GPSValues[1] = values->altitude;
  GPSValues[2] = values->longitude;
  broadcastTransform();
}

void inertialUnitCallback(const sensor_msgs::Imu::ConstPtr &values) {
  inertialUnitValues[0] = values->orientation.x;
  inertialUnitValues[1] = values->orientation.y;
  inertialUnitValues[2] = values->orientation.z;
  inertialUnitValues[3] = values->orientation.w;
  broadcastTransform();
}

void lidarCallback(const sensor_msgs::LaserScan::ConstPtr &scan) {
  int scanSize = scan->ranges.size();
  lidarValues.resize(scanSize);
  for (int i = 0; i < scanSize; ++i)
    lidarValues[i] = scan->ranges[i];

  rplidarResolution = scanSize;
  halfResolution = scanSize / 2;
  maxRange = scan->range_max;
  rangeThreshold = maxRange / 20.0;
  if (!areBraitenbergCoefficientsinitialized) {
    braitenbergCoefficients.resize(rplidarResolution);
    for (int i = 0; i < rplidarResolution; ++i)
      braitenbergCoefficients[i] = gaussian(i, halfResolution, rplidarResolution / 5);
    areBraitenbergCoefficientsinitialized = true;
  }

  updateSpeed();
}

void lidarCallsback(const sensor_msgs::LaserScan::ConstPtr &scan) {
  int scanSize = scan->ranges.size();
  lidarValues.resize(scanSize);
  for (int i = 0; i < scanSize; ++i)
    lidarValues[i] = scan->ranges[i];

  rplidarResolution = scanSize;
  halfResolution = scanSize / 2;
  maxRange = scan->range_max;
  rangeThreshold = maxRange / 20.0;
  if (!areBraitenbergCoefficientsinitialized) {
    braitenbergCoefficients.resize(rplidarResolution);
    for (int i = 0; i < rplidarResolution; ++i)
      braitenbergCoefficients[i] = gaussian(i, halfResolution, rplidarResolution / 5);
    areBraitenbergCoefficientsinitialized = true;
  }

  updateSpeed();
}


// catch names of the controllers availables on ROS network
void controllerNameCallback(const std_msgs::String::ConstPtr &name) {
  controllerCount++;
  controllerList.push_back(name->data);
  ROS_INFO("Controller #%d: %s.", controllerCount, controllerList.back().c_str());
}

void quit(int sig) {
  ROS_INFO("User stopped the 'Littlehelper8' node.");
  timeStepSrv.request.value = 0;
  timeStepClient.call(timeStepSrv);
  ros::shutdown();
  exit(0);
}

int main(int argc, char **argv) {
  std::string controllerName;
  // create a node named 'lh8' on ROS network
  ros::init(argc, argv, "Littlehelper8", ros::init_options::AnonymousName);
  ros::init(argc, argv, "UR3egrip", ros::init_options::AnonymousName);

  n = new ros::NodeHandle;

  signal(SIGINT, quit);

  // subscribe to the topic model_name to get the list of availables controllers
  ros::Subscriber nameSub = n->subscribe("model_name", 100, controllerNameCallback);
  while (controllerCount == 0 || controllerCount < nameSub.getNumPublishers()) {
    ros::spinOnce();
    ros::spinOnce();
    ros::spinOnce();
  }
  ros::spinOnce();

  timeStepClient = n->serviceClient<webots_ros::set_int>("Littlehelper8/robot/time_step");
  timeStepSrv.request.value = TIME_STEP;

  // if there is more than one controller available, it let the user choose
  if (controllerCount == 2)
    controllerName = controllerList[0];
  else {
    int wantedController = 0;
    std::cout << "Choose the # of the controller you want to use:\n";
    std::cin >> wantedController;
    if (2 <= wantedController && wantedController <= controllerCount)
      controllerName = controllerList[wantedController - 2];
    else {
      ROS_ERROR("Invalid number for controller choice.");
      return 1;
    }
  }
  ROS_INFO("Using controller: '%s'", controllerName.c_str());
  // leave topic once it is not necessary anymore
  nameSub.shutdown();

  // init motors
  for (int i = 0; i < NMOTORS; ++i) {
    // position
    ros::ServiceClient set_position_client;
    webots_ros::set_float set_position_srv;
    set_position_client = n->serviceClient<webots_ros::set_float>(std::string("Littlehelper8/") + std::string(motorNames[i]) +
                                                                  std::string("/set_position"));

    set_position_srv.request.value = INFINITY;
    if (set_position_client.call(set_position_srv) && set_position_srv.response.success)
      ROS_INFO("Position set to INFINITY for motor %s.", motorNames[i]);
    else
      ROS_ERROR("Failed to call service set_position on motor %s.", motorNames[i]);

    // speed
    ros::ServiceClient set_velocity_client;
    webots_ros::set_float set_velocity_srv;
    set_velocity_client = n->serviceClient<webots_ros::set_float>(std::string("Littlehelper8/") + std::string(motorNames[i]) +
                                                                  std::string("/set_velocity"));

    set_velocity_srv.request.value = 0.0;
    if (set_velocity_client.call(set_velocity_srv) && set_velocity_srv.response.success == 1)
      ROS_INFO("Velocity set to 0.0 for motor %s.", motorNames[i]);
    else
      ROS_ERROR("Failed to call service set_velocity on motor %s.", motorNames[i]);
  }

  // enable lidar
  ros::ServiceClient set_lidar_client;
  webots_ros::set_int lidar_srv;
  ros::Subscriber sub_lidar_scan;
  set_lidar_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/LDS_1/enable");
  lidar_srv.request.value = TIME_STEP;
  if (set_lidar_client.call(lidar_srv) && lidar_srv.response.success) {
    ROS_INFO("Lidar1 enabled.");
    sub_lidar_scan = n->subscribe("Littlehelper8/LDS_1/laser_scan/layer0", 10, lidarCallback);
  //  sub_lidar_scan = n->subscribe("Littlehelper8/LDS_2/laser_scan/layer1", 10, lidarCallback);
    ROS_INFO("Topic for lidar1 initialized.");
    while (sub_lidar_scan.getNumPublishers() == 0) {
    }
    ROS_INFO("Topic for lidar1 scan connected.");
  } else {
    if (!lidar_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable lidar1.");
    return 1;
  }

  set_lidar_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/LDS_2/enable");
  lidar_srv.request.value = TIME_STEP;
  if (set_lidar_client.call(lidar_srv) && lidar_srv.response.success) {
    ROS_INFO("Lidar2 enabled.");
    sub_lidar_scan = n->subscribe("Littlehelper8/LDS_2/laser_scan/layer0", 10, lidarCallsback);
  //  sub_lidar_scan = n->subscribe("Littlehelper8/LDS_2/laser_scan/layer1", 10, lidarCallback);
    ROS_INFO("Topic for lidar2 initialized.");
    while (sub_lidar_scan.getNumPublishers() == 0) {
    }
    ROS_INFO("Topic for lidar2 scan connected.");
  } else {
    if (!lidar_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable lidar2.");
    return 1;
  }

  // set_lidar_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/LDS_2/enable");
  // lidar_srv.request.value = TIME_STEP;
  // set_lidar_client.call(lidar_srv);
  //   ROS_INFO("Lidar2 enabled.");
  //   ROS_INFO("Topic for lidar2 initialized.");

  // enable gps
  ros::ServiceClient set_GPS_client;
  webots_ros::set_int GPS_srv;
  ros::Subscriber sub_GPS;
  set_GPS_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/gps_lh8/enable");
  GPS_srv.request.value = 32;
  if (set_GPS_client.call(GPS_srv) && GPS_srv.response.success) {
    sub_GPS = n->subscribe("Littlehelper8/gps_lh8/values", 1, GPSCallback);
    while (sub_GPS.getNumPublishers() == 0) {
    }
    ROS_INFO("GPS enabled.");
  } else {
    if (!GPS_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable GPS.");
    return 1;
  }

//  enable inertial unit
  ros::ServiceClient set_inertial_unit_client;
  webots_ros::set_int inertial_unit_srv;
  ros::Subscriber sub_inertial_unit;
  set_inertial_unit_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/inertial_unit_lh8/enable");
  inertial_unit_srv.request.value = 64;
  if (set_inertial_unit_client.call(inertial_unit_srv) && inertial_unit_srv.response.success) {
    sub_inertial_unit = n->subscribe("Littlehelper8/inertial_unit_lh8/roll_pitch_yaw", 1, inertialUnitCallback);
    while (sub_inertial_unit.getNumPublishers() == 0) {
    }
    ROS_INFO("Inertial unit enabled.");
  } else {
    if (!inertial_unit_srv.response.success)
      ROS_ERROR("Sampling period is not valid.");
    ROS_ERROR("Failed to enable inertial unit.");
    return 1;
  }

  // enable accelerometer
  ros::ServiceClient set_accelerometer_client;
  webots_ros::set_int accelerometer_srv;
  ros::Subscriber sub_accelerometer;
  set_accelerometer_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/accelerometer_lh8/enable");
  accelerometer_srv.request.value = 32;
  set_accelerometer_client.call(accelerometer_srv);
   ROS_INFO("Accelerometer enabled.");

  // enable gyro
   ros::ServiceClient set_gyro_client;
   webots_ros::set_int gyro_srv;
   ros::Subscriber sub_gyro;
   set_gyro_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/gyro_lh8/enable");
   gyro_srv.request.value = 32;
   set_gyro_client.call(gyro_srv);
   ROS_INFO("Gyro enabled.");

  // enable camera
  // ros::ServiceClient set_camera_client;
  // webots_ros::set_int camera_srv;
  // ros::Subscriber sub_camera;
  // set_camera_client = n->serviceClient<webots_ros::set_int>("Littlehelper8/camera_lh8/enable");
  // camera_srv.request.value = 64;
  // set_camera_client.call(camera_srv);

  // enable multisensebase + (topics can be enabled)
  ros::ServiceClient set_camera_client;
  webots_ros::set_int camera_srv;
  ros::Subscriber sub_camera;
  set_camera_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/MultiSensebase_left_camera/enable");
  camera_srv.request.value = 64;
  set_camera_client.call(camera_srv);
  ROS_INFO("MultiSensebase_left_camera enabled.");

  set_camera_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/MultiSensebase_right_camera/enable");
  camera_srv.request.value = 64;
  set_camera_client.call(camera_srv);
  ROS_INFO("MultiSensebase_right_camera enabled.");

  set_camera_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/MultiSensebase_inertial_unit/enable");
  camera_srv.request.value = 64;
  set_camera_client.call(camera_srv);
  ROS_INFO("MultiSensebase_inertial_unit.");

  set_camera_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/MultiSensebase_meta_camera/enable");
  camera_srv.request.value = 64;
  set_camera_client.call(camera_srv);
  ROS_INFO("MultiSensebase_meta_camera.");

  set_camera_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/MultiSensebase_meta_range_finder/enable");
  camera_srv.request.value = 64;
  set_camera_client.call(camera_srv);
  ROS_INFO("MultiSensebase_meta_range_finder.");

  //enable multisensearm (topics can be enabled)
  set_camera_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/MultiSensearm_left_camera/enable");
  camera_srv.request.value = 64;
  set_camera_client.call(camera_srv);
  ROS_INFO("MultiSensearm_left_camera.");

  set_camera_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/MultiSensearm_right_camera/enable");
  camera_srv.request.value = 64;
  set_camera_client.call(camera_srv);
  ROS_INFO("MultiSensearm_right_camera.");

  set_camera_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/MultiSensearm_meta_camera/enable");
  camera_srv.request.value = 64;
  set_camera_client.call(camera_srv);
  ROS_INFO("MultiSensearm_meta_camera.");

  set_camera_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/MultiSensearm_inertial_unit/enable");
  camera_srv.request.value = 64;
  set_camera_client.call(camera_srv);
  ROS_INFO("MultiSensearm_inertial_unit.");

  set_camera_client = n->serviceClient<webots_ros::set_int>("/UR3egrip/MultiSensearm_meta_range_finder/enable");
  camera_srv.request.value = 64;
  set_camera_client.call(camera_srv);
  ROS_INFO("MultiSensearm_meta_range_finder.");


   //turn on leds
  ros::ServiceClient set_led_client;
  webots_ros::set_int set_led_srv;
  set_led_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/led1/set_led");

  set_led_srv.request.value = 1;
  if (set_led_client.call(set_led_srv) && set_led_srv.response.success)
    ROS_INFO("LED set to 1.");
  else
    ROS_ERROR("Failed to call service set_led.");

  set_led_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/led2/set_led");

  set_led_srv.request.value = 1;
  if (set_led_client.call(set_led_srv) && set_led_srv.response.success)
    ROS_INFO("LED set to 2.");
  else
    ROS_ERROR("Failed to call service set_led.");

  set_led_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/led3/set_led");

  set_led_srv.request.value = 1;
  if (set_led_client.call(set_led_srv) && set_led_srv.response.success)
    ROS_INFO("LED set to 3.");
  else
    ROS_ERROR("Failed to call service set_led.");

  set_led_client = n->serviceClient<webots_ros::set_int>("/Littlehelper8/led4/set_led");

  set_led_srv.request.value = 1;
  if (set_led_client.call(set_led_srv) && set_led_srv.response.success)
    ROS_INFO("LED set to 4.");
  else
    ROS_ERROR("Failed to call service set_led.");



   ROS_INFO("You can now start the creation of the map using 'rosrun gmapping slam_gmapping "
           "scan:=/Littlehelper8/LDS_1/laser_scan/layer0'.");

   ROS_INFO("or  You can now start the creation of the map using 'rosrun gmapping slam_gmapping "
           "scan:=/Littlehelper8/LDS_2/laser_scan/layer0'.");

   ROS_INFO("or  You can now start the creation of the map using 'rosrun gmapping slam_gmapping "
           "scan:=/scan_multi'.");

   ROS_INFO("You can now visualize the sensors output in rqt using 'rqt'.");

  // main loop
  while (ros::ok()) {
    if (!timeStepClient.call(timeStepSrv) || !timeStepSrv.response.success) {
      ROS_ERROR("Failed to call service time_step for next step.");
      break;
    }
    ros::spinOnce();
  }
  timeStepSrv.request.value = 0;
  timeStepClient.call(timeStepSrv);

  ros::shutdown();
  return 0;
}
