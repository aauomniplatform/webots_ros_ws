#! /usr/bin/env python

import sys
import copy
import rospy
import numpy as np
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from tf import transformations
import tf2_ros
import tf2_geometry_msgs
from geometry_msgs.msg import PoseStamped, Point
from sensor_msgs.msg import NavSatFix, Imu
import actionlib
from math import pi,cos,sin,tan,atan2,sqrt
from collections import deque
from webots_ros.srv import set_float


base_loc = np.array([[1.],[1.],[0.]])
base_yaw =0
ur_height = 0.66
theta_6=0

ze = np.zeros([3,1])
Int = deque()
Int.appendleft(ze)

timestep = 32

def get_object_pose():
	pose_target = geometry_msgs.msg.Pose()

	q = transformations.quaternion_from_euler(0,pi,0)

	pose_target.orientation.x = q[0]
	pose_target.orientation.y = q[1]
	pose_target.orientation.z = q[2]
	pose_target.orientation.w = q[3]

	pose_target.position.x=-4.7
	pose_target.position.y=0
	pose_target.position.z=0.771
	
	return pose_target

def gripper(set):
	if set:
		gripper_finger_1(0.5)
		gripper_finger_2(0.5)
		gripper_finger_3(0.5)
		gripper_finger_12(1)
		gripper_finger_22(1)
		gripper_finger_32(1)
	else:
		gripper_finger_1(0.0)
		gripper_finger_2(0.0)
		gripper_finger_3(0.0)
		gripper_finger_12(0.0)
		gripper_finger_22(0.0)
		gripper_finger_32(0.0)

def camera_output_pose():
	object_pose = geometry_msgs.msg.PoseStamped()
	object_pose.header.stamp = rospy.Time(0)
	object_pose.header.frame_id = "world"

	pose_target = get_object_pose()

	object_pose.pose.position.x = pose_target.position.x
	object_pose.pose.position.y = -pose_target.position.y ### beacuse the coordinate frame of tf is different from that of webots
	object_pose.pose.position.z = pose_target.position.z
	object_pose.pose.orientation = pose_target.orientation
	while True:
		try:
			object_world_pose = tf_buffer.transform(object_pose, "base_link")
			return object_world_pose
		except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
			print(e)
			continue

def move_manipulator():
	robot2_group.set_named_target("R1Up")
	robot2_group.go()
	rospy.loginfo('Go UP')

	robot2_group.set_named_target("center_straight_grasp")
	robot2_group.go()
	rospy.loginfo('Go to Pre Grasp')

	object_world_pose = camera_output_pose()
	rospy.loginfo('object pose received')

	print "Going to object pose"
	print(object_world_pose)
	object_world_pose.pose.position.z += 0.2
	robot2_group.set_pose_target(object_world_pose)
	robot2_group.go()

	curr_pose = robot2_group.get_current_pose()

	curr_pose.pose.position.x += -0.1
	robot2_group.set_pose_target(curr_pose)
	robot2_group.go()

	curr_pose.pose.position.z += -0.2
	robot2_group.set_pose_target(curr_pose)
	robot2_group.go()

	rospy.loginfo('Gripping')
	gripper(True)
	rospy.loginfo('Pick up')

	robot2_group.set_named_target("R1Up")
	robot2_group.go()
	rospy.loginfo('Go UP')

	robot2_group.set_named_target("R1place")
	robot2_group.go()
	rospy.loginfo('Placing')
	gripper(False)

def clamp(n, minn, maxn):
    if n < minn:
        return minn
    elif n > maxn:
        return maxn
    else:
        return n

def gps_callback(values):
	global base_loc
	base_loc[0] = values.latitude
	base_loc[1] = values.longitude
	base_loc[2] = values.altitude

def imu_callback(values):
	global base_yaw
	euler = transformations.euler_from_quaternion([values.orientation.x,values.orientation.y,values.orientation.z,values.orientation.w])
	base_yaw = -euler[2]

def controller_wheel(current_pose,Destination_pose,X):
    #######initilizations

    m =200
    ly =0.8
    lx = 0.5
    r = 0.127
    max_linear_speed = 20.00
    max_angular_speed = 12.00

    #######inverse kinematic Equations
    Pi = current_pose

    B = np.array([[1, 1, 1, 1],[-1, 1, 1, -1 ],[-1/(lx+ly), 1/(lx+ly), -1/(lx+ly), 1/(lx+ly)]])
    B_inv = np.array([[1, -1, -(lx+ly)/2],[1, 1, (lx+ly)/2] ,[1, 1, -(lx+ly)/2], [1, -1, (lx+ly)/2]])


    # X = Kp*np.subtract(Destination_pose,Pi)/dt

    X[0]=clamp(X[0],-max_linear_speed,max_linear_speed)
    X[1]=clamp(X[1],-max_linear_speed,max_linear_speed)
    X[2]=clamp(X[2],-max_angular_speed,max_angular_speed)

    R = np.array([[cos(Pi[2]), -sin(Pi[2]), 0],[sin(Pi[2]), cos(Pi[2]), 0],[ 0, 0, 1]])

    U_inv =X*(1/r)

    X_next_inv = np.dot(np.dot(B_inv,np.linalg.inv(R)),U_inv)


    #wheel velocities
    U =X_next_inv*(r/4)

    motor_vel = Imu()
    motor_vel.orientation.x = U[0]
    motor_vel.orientation.y = U[1]
    motor_vel.orientation.z = U[2]
    motor_vel.orientation.w = U[3]

    toprightWheelClient(U[0])
    topleftWheelClient(U[1])
    bottomrightWheelClient(U[2])
    bottomleftWheelClient(U[3])


def navigation_move_base_node(goto_location, desired_yaw):
	global timestep, Int,base_loc,base_yaw
	# print("navigation_move_base_node started/.....")
	destination = np.array([goto_location[0],goto_location[1]])
	th = desired_yaw
	Destination_pose = np.array([[goto_location[0]],[goto_location[1]],[th]])
	prev_err=0

	#gains
	Kp= 0.5
	Kd=0.01
	Ki=0.01
	####position and orientation
	v_gps = np.array([base_loc[0],base_loc[1]])

	v_dir = np.subtract(destination,v_gps)

	distance = np.linalg.norm(v_dir)

	theta =  base_yaw
	# print(theta)
	delta_theta = theta - th

	####current pose
	current_pose = np.array([[v_gps[0]],[v_gps[1]],[theta]])

	##PID
	dt = timestep*0.001
	Error = Destination_pose - current_pose
	# print("Destination_pose:")
	# print(Destination_pose)
	# print("current_pose:")
	# print(current_pose)
	# print("Error:")
	# print(Error)
	Prop = Error
	Der  = (Error - prev_err)/dt
	Int.pop()
	Int.appendleft((Error + prev_err)*dt/2)
	I = np.sum(Int,axis=1)

	PID  = Kp*Prop + Ki*I+ Kd*Der

	prev_err = Error

	X = PID/dt

	#######controller
	controller_wheel(current_pose,Destination_pose,X)

	return np.sum(abs(Error))


def base_destination_point_near_obj(pose_target):
	global base_loc

	base_circle_radius = 0.8 # reach calculated after testing

	base_obj_distance_on_plane = sqrt((pose_target.position.x - base_loc[0])**2+(pose_target.position.y - base_loc[1])**2)

	###ratio = base_circle_radius(a) : base_obj_distance_on_plane-base_circle_radius(b)
	a= base_circle_radius
	b = base_obj_distance_on_plane-base_circle_radius

	goto_x = pose_target.position.x + (a/(a+b))*(base_loc[0]- pose_target.position.x)

	goto_y = pose_target.position.y + (a/(a+b))*(base_loc[1]- pose_target.position.y)

	goto_location = np.array([[goto_x],[goto_y]])

	goto_orientation = atan2(pose_target.position.y-goto_y,pose_target.position.x-goto_x)

	return goto_location,goto_orientation

def vision_orientation(img):
	##
	#code
	##
	return theta_6

def main():
	pose_target = get_object_pose()

	goto_location,desired_yaw = base_destination_point_near_obj(pose_target)

	result =10
	rate=rospy.Rate(10)
	robot2_group.set_named_target("R1Up")
	robot2_group.go()
	rospy.loginfo('Go Up')

	print("navigation_move_base_node started/.....")

	print("goto_location:")
	print(goto_location)
	print("goto_orientation:")
	print(desired_yaw)
	while result>=0.05:
		result = navigation_move_base_node(goto_location, desired_yaw)
		rate.sleep()
	####to stop the robot
	toprightWheelClient(0)
	topleftWheelClient(0)
	bottomrightWheelClient(0)
	bottomleftWheelClient(0)
	rospy.loginfo("Goal execution done!")
	rospy.loginfo("Grabbing object!")
	move_manipulator()
	rospy.loginfo("object grabbed!")
	moveit_commander.roscpp_shutdown()


if __name__ == '__main__':
	moveit_commander.roscpp_initialize(sys.argv)
	rospy.init_node('move_group_phython_tut', anonymous=True)
	robot2_group = moveit_commander.MoveGroupCommander("manipulator")

	robot2_client = actionlib.SimpleActionClient('execute_trajectory',moveit_msgs.msg.ExecuteTrajectoryAction)
	robot2_client.wait_for_server()
	rospy.loginfo('Execute Trajectory server is available for robot2')
	# Create a TF buffer in the global scope
	tf_buffer = tf2_ros.Buffer()
	tf_listener = tf2_ros.TransformListener(tf_buffer)
	rospy.Subscriber("/Littlehelper8/inertial_unit_lh8/roll_pitch_yaw", Imu, imu_callback)
	rospy.Subscriber("/Littlehelper8/gps_lh8/values", NavSatFix, gps_callback)
	toprightWheelClient = rospy.ServiceProxy('/Littlehelper8/wheel1/set_velocity',set_float);
	topleftWheelClient = rospy.ServiceProxy('/Littlehelper8/wheel2/set_velocity',set_float);
	bottomrightWheelClient = rospy.ServiceProxy('/Littlehelper8/wheel3/set_velocity',set_float);
	bottomleftWheelClient = rospy.ServiceProxy('/Littlehelper8/wheel4/set_velocity',set_float);

	gripper_finger_1 = rospy.ServiceProxy('/UR3egrip/finger_1_joint_1/set_position',set_float)
	gripper_finger_2 = rospy.ServiceProxy('/UR3egrip/finger_2_joint_1/set_position',set_float)
	gripper_finger_3 = rospy.ServiceProxy('/UR3egrip/finger_middle_joint_1/set_position',set_float)

	gripper_finger_12 = rospy.ServiceProxy('/UR3egrip/finger_1_joint_2/set_position',set_float)
	gripper_finger_22 = rospy.ServiceProxy('/UR3egrip/finger_2_joint_2/set_position',set_float)
	gripper_finger_32 = rospy.ServiceProxy('/UR3egrip/finger_middle_joint_2/set_position',set_float)

	main()
