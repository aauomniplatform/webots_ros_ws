#! /usr/bin/env python

import sys
import copy
import rospy
import numpy as np
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from tf import transformations
from geometry_msgs.msg import PoseStamped, Point
from sensor_msgs.msg import NavSatFix, Imu
import actionlib
from math import cos,sin,sqrt
from collections import deque

base_loc = np.array([[1.],[1.],[0.]])
base_yaw =0
ur_height = 0.66
theta_6=0

ze = np.zeros([3,1])
Int = deque()
Int.appendleft(ze)

timestep = 32

def clamp(n, minn, maxn):
    if n < minn:
        return minn
    elif n > maxn:
        return maxn
    else:
        return n
        
def gps_callback(values):
	global base_loc
	base_loc[0] = values.latitude
	base_loc[1] = values.longitude
	base_loc[2] = values.altitude
	# print("base_loc:")
	# print(base_loc)

def imu_callback(values):
	global base_yaw
	euler = transformations.euler_from_quaternion([values.orientation.x,values.orientation.y,values.orientation.z,values.orientation.w])
	base_yaw = -euler[2]
	# print("base_yaw:")
	# print(base_yaw)

def controller_wheel(current_pose,Destination_pose,X):
    #######initilizations
    
    m =200
    ly =0.8
    lx = 0.5
    r = 0.127
    max_linear_speed = 12.00
    max_angular_speed = 4.00
    
    #######inverse kinematic Equations
    Pi = current_pose
    
    B = np.array([[1, 1, 1, 1],[-1, 1, 1, -1 ],[-1/(lx+ly), 1/(lx+ly), -1/(lx+ly), 1/(lx+ly)]])
    B_inv = np.array([[1, -1, -(lx+ly)/2],[1, 1, (lx+ly)/2] ,[1, 1, -(lx+ly)/2], [1, -1, (lx+ly)/2]])
    
    
    # X = Kp*np.subtract(Destination_pose,Pi)/dt
    
    X[0]=clamp(X[0],-max_linear_speed,max_linear_speed)
    X[1]=clamp(X[1],-max_linear_speed,max_linear_speed)
    X[2]=clamp(X[2],-max_angular_speed,max_angular_speed)
    
    R = np.array([[cos(Pi[2]), -sin(Pi[2]), 0],[sin(Pi[2]), cos(Pi[2]), 0],[ 0, 0, 1]])

    U_inv =X*(1/r)
    
    X_next_inv = np.dot(np.dot(B_inv,np.linalg.inv(R)),U_inv)
    
    
    #wheel velocities 
    U =X_next_inv*(r/4)

    motor_vel = Imu()
    motor_vel.orientation.x = U[0]
    motor_vel.orientation.y = U[1]
    motor_vel.orientation.z = U[2]
    motor_vel.orientation.w = U[3]
    
    return motor_vel


def navigation_move_base_node(goto_location):
	global timestep, Int,base_loc,base_yaw
	# print("navigation_move_base_node started/.....")
	destination = np.array([goto_location[0],goto_location[1]])
	th =0
	Destination_pose = np.array([[goto_location[0]],[goto_location[1]],[th]])
	prev_err=0

	#gains
	Kp= 0.5
	Kd=0.01
	Ki=0.01
	####position and orientation
	v_gps = np.array([base_loc[0],base_loc[1]])

	v_dir = np.subtract(destination,v_gps)

	distance = np.linalg.norm(v_dir)

	theta =  base_yaw
	# print(theta)
	delta_theta = theta - th

	####current pose
	current_pose = np.array([[v_gps[0]],[v_gps[1]],[theta]])

	##PID
	dt = timestep*0.001
	Error = Destination_pose - current_pose
	# print("Destination_pose:")
	# print(Destination_pose)
	# print("current_pose:")
	# print(current_pose)
	# print("error:")
	# print(Error)

	Prop = Error
	Der  = (Error - prev_err)/dt
	Int.pop()
	Int.appendleft((Error + prev_err)*dt/2)
	I = np.sum(Int,axis=1)

	PID  = Kp*Prop + Ki*I+ Kd*Der

	prev_err = Error

	X = PID/dt

	#######controller
	motor_vel = controller_wheel(current_pose,Destination_pose,X)

	motor_vel_pub.publish(motor_vel)
	# print("motor_vel:")
	# print(motor_vel)
	return np.sum(Error)


def object_local_coordinate_transformation(pose_target,theta_6,goto_location):
	global base_loc, base_yaw,ur_height
	ur_loc = np.array([[0.],[0.],[0.]])
	ur_loc[2] = ur_height
	x_offset=0.28
	ur_loc[0] = base_loc[0]-x_offset###these should later be subscribes real time
	ur_loc[1] = base_loc[1]###
	world_to_robot_trans = np.array([[cos(base_yaw),-sin(base_yaw),0],[sin(base_yaw),cos(base_yaw),0],[0,0,1]])
	obj_pos = np.array([[pose_target.position.x],[pose_target.position.y],[pose_target.position.z]])

	obj_pos_local = np.dot(np.linalg.inv(world_to_robot_trans),(obj_pos - ur_loc))
	obj_pos_in_ur_frame = geometry_msgs.msg.Pose()
	obj_pos_in_ur_frame.position.x=float(obj_pos_local[0])
	obj_pos_in_ur_frame.position.y=float(obj_pos_local[1])
	obj_pos_in_ur_frame.position.z=float(obj_pos_local[2])

	# q = transformations.quaternion_from_euler(theta_6, 0, 0, "sxyz") # r p y

	obj_pos_in_ur_frame.orientation.x = q[0]
	obj_pos_in_ur_frame.orientation.y = q[1]
	obj_pos_in_ur_frame.orientation.z = q[2]
	obj_pos_in_ur_frame.orientation.w = q[3]
	#####test position
	# obj_pos_in_ur_frame.position.x=0.3
	# obj_pos_in_ur_frame.position.y=0.3
	# obj_pos_in_ur_frame.position.z=0.4
	#########
	print("obj_pos_in_ur_frame:")
	print(obj_pos_in_ur_frame)
	return obj_pos_in_ur_frame

def base_destination_point_near_obj(pose_target):
	global base_loc,ur_height
	ur_reach = 0.5 # it is 0.85, but take 0.8 for safe condition
	# base_loc[2] = ur_height
	##plane eq == z=0.65
	base_circle_radius = sqrt(ur_reach**2 - (ur_height-pose_target.position.z)**2)

	base_obj_distance_on_plane = sqrt((pose_target.position.x - base_loc[0])**2+(pose_target.position.y - base_loc[1])**2)

	###ratio = base_circle_radius(a) : base_obj_distance_on_plane-base_circle_radius(b)
	a= base_circle_radius
	b = base_obj_distance_on_plane-base_circle_radius

	goto_x = pose_target.position.x + (a/(a+b))*(base_loc[0]- pose_target.position.x)

	goto_y = pose_target.position.y + (a/(a+b))*(base_loc[1]- pose_target.position.y)

	goto_location = np.array([[goto_x],[goto_y]])
	
	print("goto_location:")
	print(goto_location)
	return goto_location

def vision_orientation(img):
	##
	#code
	##
	return theta_6

def base_position_get(pose_target, theta):
	###########################
	#calculation of base frame location from end effector

	####end effector location
	e_x =group.get_current_pose().pose.position.x
	e_y =group.get_current_pose().pose.position.y
	e_z =group.get_current_pose().pose.position.z

	end_effector_location = np.array([[e_x],[e_y],[e_z],[1.00]])

	##dh parameters for ur3
	d1 =  0.1519;
	a2 = -0.24365;
	a3 = -0.21325;
	d4 =  0.11235;
	d5 =  0.08535;
	d6 =  0.0819;
	a = np.array([0,0,a2,a3,0,0])
	d = np.array([d1,0,0,d4,d5,d6])
	alpha = np.array([1.57,0,0,1.57,-1.57,0])

	#joint angles from theta1-6
	# theta = group.get_current_joint_values()
	
	###kinematics
	s1 = sin(theta[0])
	c1 = cos(theta[0])
	q23 = theta[1]
	q234 = theta[1]
	s2 = sin(theta[1])
	c2 = cos(theta[1])
	s3 = sin(theta[2])
	c3 = cos(theta[2])
	q23 += theta[2]
	q234 += theta[2]
	s4 = sin(theta[3])
	c4 = cos(theta[3])
	q234 += theta[3]
	s5 = sin(theta[4])
	c5 = cos(theta[4])
	s6 = sin(theta[5])
	c6 = cos(theta[5])
	s23 = sin(q23)
	c23 = cos(q23)
	s234 = sin(q234)
	c234 = cos(q234)
	###transformation matrix
	T = np.array([[c234*c1*s5 - c5*s1,c6*(s1*s5 + c234*c1*c5) - s234*c1*s6,-s6*(s1*s5 + c234*c1*c5) - s234*c1*c6,d6*c234*c1*s5 - a3*c23*c1 - a2*c1*c2 - d6*c5*s1 - d5*s234*c1 - d4*s1], \
				[c1*c5 + c234*s1*s5,-c6*(c1*s5 - c234*c5*s1) - s234*s1*s6,s6*(c1*s5 - c234*c5*s1) - s234*c6*s1,d6*(c1*c5 + c234*s1*s5) + d4*c1 - a3*c23*s1 - a2*c2*s1 - d5*s234*s1],\
				[-s234*s5,-c234*s6 - s234*c5*c6,s234*c5*s6 - c234*c6,d1 + a3*s23 + a2*s2 - d5*(c23*c4 - s23*s4) - d6*s5*(c23*s4 + s23*c4)],\
				[0.0,0.0,0.0,1.0]])
	#########to get base location
	T_inv = np.linalg.inv(T) 

	Base = np.dot(T_inv,end_effector_location)

	print "Robot base location: %s" % Base

	return Base

def move_manipulator(obj_pos_in_ur_frame):
	group.set_pose_target(obj_pos_in_ur_frame)

	plan1 = group.plan()

	group.go(wait=True)





moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_phython_tut', anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group = moveit_commander.MoveGroupCommander("manipulator")
display_trajectory_publisher = rospy.Publisher('move_group/display_planned_path',moveit_msgs.msg.DisplayTrajectory, queue_size=1)
rospy.Subscriber("/Littlehelper8/inertial_unit_lh8/roll_pitch_yaw", Imu, imu_callback)
rospy.Subscriber("/Littlehelper8/gps_lh8/values", NavSatFix, gps_callback)
motor_vel_pub = rospy.Publisher('motor_vel',Imu, queue_size=1)



pose_target = geometry_msgs.msg.Pose()

q = transformations.quaternion_from_euler(0, 0, 0, "sxyz") # r p y

pose_target.orientation.x = q[0]
pose_target.orientation.y = q[1]
pose_target.orientation.z = q[2]
pose_target.orientation.w = q[3]

pose_target.position.x=2
pose_target.position.y=2
pose_target.position.z=1.06


# print "joint values: %s" % group.get_current_joint_values()
#default joint values
theta = np.array([0.7065433553865974, -0.9388140232246958, 1.007216068133884, 3.0723168516139676, -2.2771523891760097, -0.0009641151009898152])

goto_location = base_destination_point_near_obj(pose_target)

result =10
rate=rospy.Rate(10)
print("navigation_move_base_node started/.....")
while result>=0.1:
	result = navigation_move_base_node(goto_location)
	# rospy.spin()
	rate.sleep()
####to stop the robot
zero_vel = Imu()
zero_vel.orientation.x = 0
zero_vel.orientation.y = 0
zero_vel.orientation.z = 0
zero_vel.orientation.w = 0
motor_vel_pub.publish(zero_vel)
rospy.loginfo("Goal execution done!")
rospy.loginfo("Grabbing object!")
rounded_pose_target = object_local_coordinate_transformation(pose_target, theta_6,goto_location)
rounded_pose_target.position.x = round(rounded_pose_target.position.x,2)
rounded_pose_target.position.y = round(rounded_pose_target.position.y,2)
rounded_pose_target.position.z = round(rounded_pose_target.position.z,2)
move_manipulator(rounded_pose_target)
rospy.loginfo("object grabbed!")


# base_position_get(pose_target, theta)
# move_manipulator(pose_target)
# object_local_coordinate_transformation(pose_target)
# base_destination_point_near_obj(pose_target)
# group_variable_values = group.get_current_joint_values()

# group_variable_values[0] = 1
# group_variable_values[1] = 0
# group_variable_values[3] = -1.5
# group_variable_values[5] = 1.5

# group.set_joint_value_target(group_variable_values)

# rospy.sleep(5)

moveit_commander.roscpp_shutdown()
